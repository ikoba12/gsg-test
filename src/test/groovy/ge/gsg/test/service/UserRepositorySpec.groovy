package ge.gsg.test.service

import ge.gsg.test.model.entity.AppUser
import ge.gsg.test.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

import java.text.SimpleDateFormat

@SpringBootTest
class UserRepositorySpec extends Specification {

    @Autowired
    UserRepository userRepository


    def 'when querying for users should return correct number of users'() {
        when :'difference is 5 minutes '
        AppUser u = new AppUser()
        u.setJobRunInterval 3
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm")
        Date lastQuery = format.parse("2019/10/17 16:10")
        Date now = format.parse("2019/10/17 16:15")
        u.setLastQueryTime lastQuery.getTime()
        userRepository.save u
        List<AppUser> appUserList = userRepository.findAllByValidDate(now.getTime())
        then: 'should return user whose interval is 3'
        appUserList.size() == 1
        when: 'adding another user whose interval is longer then 5 minutes '
        u = new AppUser()
        u.setLastQueryTime lastQuery.getTime()
        u.setJobRunInterval(6)
        userRepository.save(u)
        appUserList = userRepository.findAllByValidDate(now.getTime())
        then : 'should still return user whose interval is less then 5'
        appUserList.size() == 1
        when : 'increasing time passed to 7 minutes'
        now = format.parse("2019/10/17 16:17")
        appUserList = userRepository.findAllByValidDate(now.getTime())
        then: 'should return both users'
        appUserList.size() ==2

    }
}
