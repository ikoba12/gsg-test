package ge.gsg.test.repository;

import ge.gsg.test.model.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<AppUser,Long> {

    @Query("Select a from AppUser a where  (a.lastQueryTime IS NULL OR (((:currentDate - a.lastQueryTime)/60000)%60> a.jobRunInterval))")
    List<AppUser> findAllByValidDate(@Param("currentDate") Long currentDate);

    Optional<AppUser> findByUsername(String username);
}
