package ge.gsg.test.repository;

import ge.gsg.test.model.entity.Media;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MediaRepository extends JpaRepository<Media,Long> {
    List<Media> findAllByAppUserId(Long userId);
}
