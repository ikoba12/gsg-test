package ge.gsg.test.model.exception;

import lombok.Getter;

@Getter
public class GeneralException extends RuntimeException {

    private int code;

    public GeneralException(int code, String message) {
        super(message);
        this.code = code;
    }

    public GeneralException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}
