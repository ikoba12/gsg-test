package ge.gsg.test.model.exception;

public class MediaDownloadException extends Exception {


    public MediaDownloadException() {
    }

    public MediaDownloadException(String message, Throwable cause) {
        super(message, cause);
    }

    public MediaDownloadException(String message) {
        super(message);
    }
}
