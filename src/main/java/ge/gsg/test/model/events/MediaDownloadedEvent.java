package ge.gsg.test.model.events;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
@ToString
public class MediaDownloadedEvent extends ApplicationEvent {

    private String videoUrl;

    private String commentUrl;

    private Long userId;

    public MediaDownloadedEvent(Object source, String videoUrl, String commentUrl, Long userId) {
        super(source);
        this.videoUrl = videoUrl;
        this.commentUrl = commentUrl;

    }


}
