package ge.gsg.test.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;



@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString(exclude = {"mediaList"})
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "app_user_id_seq")
    @SequenceGenerator(name = "app_user_id_seq", sequenceName = "app_user_id_seq", allocationSize = 1, initialValue = 5)
    private Long id;

    @Column(unique = true)
    private String username;

    private String password;

    private String country;

    private Integer jobRunInterval;

    @OneToMany(mappedBy = "appUser", fetch = FetchType.LAZY)
    private List<Media> mediaList;

    private Long lastQueryTime;

    public AppUser(Long id) {
        this.id = id;
    }
}
