package ge.gsg.test.model.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter

public class UpdateUserRequest {

    private String country;
    @Min(1)
    @Max(60)
    private Integer interval;
}
