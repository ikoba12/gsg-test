package ge.gsg.test.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ToString
public class LoginUserRequest {

    @NotEmpty
    private String username;

    @NotEmpty
    private String password;
}
