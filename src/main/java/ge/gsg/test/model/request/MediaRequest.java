package ge.gsg.test.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@ToString
public class MediaRequest {

    @NotEmpty
    private Long userId;

}
