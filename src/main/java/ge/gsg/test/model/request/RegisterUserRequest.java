package ge.gsg.test.model.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class RegisterUserRequest{

    @NotEmpty(message = "Username is a required field")
    private String username;

    @NotEmpty(message = "Password is a required field")
    private String password;

    @NotEmpty(message = "Country is a required field")
    private String country;

    @NotNull
    @Min(value = 1, message = "Interval must be greater than zero")
    @Max(value = 60, message = "Interval must be less than 60")
    private Integer interval;

}
