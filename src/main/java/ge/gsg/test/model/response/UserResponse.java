package ge.gsg.test.model.response;

import ge.gsg.test.model.entity.AppUser;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
@ToString
public class UserResponse {
    private Long id;

    private String username;

    private String country;

    private Integer jobRunInterval;

    public static UserResponse fromAppUser(AppUser appUser){
        UserResponse userResponse = new UserResponse();
        BeanUtils.copyProperties(appUser, userResponse);
        return userResponse;
    }
}
