package ge.gsg.test.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class YoutubeVideoResponse {
    private String title;
    private String url;
    private String thumbnailUrl;
    private String publishDate;
    private String description;
}
