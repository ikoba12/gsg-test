package ge.gsg.test.model.enums;

import lombok.Getter;

@Getter
public enum GeneralErrorEnum {
    REGISTRATION_FAILURE(1, "AppUser Registration failed"),
    UPDATE_FAILURE(2, "AppUser Update failed"),
    USER_NOT_FOUND(3, "User with given Id not found"),
    MEDIA_GET_FAILURE(4, "Error while getting media for user"),
    INVALID_PASSWORD(5,"Invalid password"),
    LOGIN_ERROR(6, "Error while logging in"),
    USER_ALREADY_EXIST(7, "User with given Id already exists"),
    UPDATE_INVALID_PARAMS(8, "At least one field must be present");
    private int code;

    private String message;

    GeneralErrorEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }


}
