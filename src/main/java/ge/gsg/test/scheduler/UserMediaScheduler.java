package ge.gsg.test.scheduler;

import ge.gsg.test.model.entity.AppUser;
import ge.gsg.test.repository.UserRepository;
import ge.gsg.test.service.MediaDownloadService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class UserMediaScheduler {


    private MediaDownloadService mediaDownloadService;

    private UserRepository userRepository;

    @Scheduled(cron = "${scheduler.user.media}")
    public void updateUserMedia(){
        try{
            log.info("Running update user media job");
            Date date = new Date();
            List<AppUser> users = userRepository.findAllByValidDate(date.getTime());
            users.forEach(user ->{
                try{
                    log.info("Downloading media for user with id : {}", user.getId());
                    mediaDownloadService.downloadMediaForUser(user);
                    user.setLastQueryTime(new Date().getTime());
                    userRepository.save(user);
                }catch (Exception e){
                    log.error("Error while downloading media for user : {}", user);
                }
            });

        }catch (Exception e){
            log.error("Error while updating user media",e);
        }
    }

    @Autowired
    public void setMediaDownloadService(MediaDownloadService mediaDownloadService) {
        this.mediaDownloadService = mediaDownloadService;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
