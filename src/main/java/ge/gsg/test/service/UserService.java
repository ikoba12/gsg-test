package ge.gsg.test.service;

import ge.gsg.test.model.exception.InvalidUsernamePasswordException;
import ge.gsg.test.model.exception.UserNotFoundException;
import ge.gsg.test.model.request.LoginUserRequest;
import ge.gsg.test.model.request.RegisterUserRequest;
import ge.gsg.test.model.request.UpdateUserRequest;
import ge.gsg.test.model.response.UserResponse;

public interface UserService {
    void registerUser(RegisterUserRequest request) throws UserNotFoundException;

    void updateUser(Long userId, UpdateUserRequest request) throws UserNotFoundException;

    UserResponse loginUser(LoginUserRequest request) throws InvalidUsernamePasswordException;
}
