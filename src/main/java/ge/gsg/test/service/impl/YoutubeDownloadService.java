package ge.gsg.test.service.impl;

import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeRequest;
import com.google.api.services.youtube.YouTubeRequestInitializer;
import com.google.api.services.youtube.model.*;
import ge.gsg.test.model.entity.AppUser;
import ge.gsg.test.model.events.MediaDownloadedEvent;
import ge.gsg.test.model.exception.MediaDownloadException;
import ge.gsg.test.model.response.YoutubeVideoResponse;
import ge.gsg.test.service.MediaDownloadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class YoutubeDownloadService implements MediaDownloadService {
    private static final String YOUTUBE_VIDEO_LINK_BASE_URL = "https://www.youtube.com/embed/v=";

    @Value("${youtube.api.key}")
    private String apiKey;

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void downloadMediaForUser(AppUser appUser) {
        log.info("Downloading Youtube media for user {}" ,appUser);
        try {
            YouTube youtube = getYouTube();
            YouTube.Videos.List search = youtube.videos().list("id");
            search.setRegionCode(appUser.getCountry());
            search.setChart("mostPopular");
            search.setKey(apiKey);
            search.setMaxResults(1L);
            VideoListResponse searchResponse = search.execute();
            List<Video> searchResultList = searchResponse.getItems();
            if (!CollectionUtils.isEmpty(searchResultList)) {
                Optional<Video> videoOptional = searchResultList.stream().findAny();
                Video video = videoOptional.orElseThrow(MediaDownloadException::new);
                Optional<String> commentIdOptional = downloadTopCommentForVideo(video.getId());
                String commentId = commentIdOptional.orElseThrow(() -> new MediaDownloadException("Error:  No comments found"));
                String videoUrl = String.format("%s%s",YOUTUBE_VIDEO_LINK_BASE_URL, video.getId());
                MediaDownloadedEvent event = new MediaDownloadedEvent(this,videoUrl,commentId,appUser.getId());
                applicationEventPublisher.publishEvent(event);
            }
        } catch (Exception e) {
            log.error("Error while downloading media" , e);
        }

    }

    private Optional<String> downloadTopCommentForVideo(String videoId) throws MediaDownloadException {
        try{
            YouTube.CommentThreads.List search = getYouTube().commentThreads().list("id");
            search.setKey(apiKey);
            search.setVideoId(videoId);
            CommentThreadListResponse searchResponse = search.execute();
            List<CommentThread> searchResultList = searchResponse.getItems();
            if (!CollectionUtils.isEmpty(searchResultList)) {
                Optional<CommentThread> commentThreadOptional= searchResultList.stream().findAny();
                CommentThread commentThread = commentThreadOptional.orElseThrow(MediaDownloadException::new);
                return Optional.of(commentThread.getId());
            }
            return Optional.empty();
        }catch (Exception e){
            log.error("Error while downloading commeents for video with id : {}",videoId, e);
            throw new MediaDownloadException("Exception while downloading comments", e);
        }
    }

    private YouTube getYouTube() {

        return new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(),
                (request) -> {}).setApplicationName("gsg-test").build();
    }

    @Autowired
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
