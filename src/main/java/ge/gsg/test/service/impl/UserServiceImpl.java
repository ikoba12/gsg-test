package ge.gsg.test.service.impl;

import ge.gsg.test.model.entity.AppUser;
import ge.gsg.test.model.enums.GeneralErrorEnum;
import ge.gsg.test.model.exception.GeneralException;
import ge.gsg.test.model.exception.InvalidUsernamePasswordException;
import ge.gsg.test.model.exception.UserNotFoundException;
import ge.gsg.test.model.request.LoginUserRequest;
import ge.gsg.test.model.request.RegisterUserRequest;
import ge.gsg.test.model.request.UpdateUserRequest;
import ge.gsg.test.model.response.UserResponse;
import ge.gsg.test.repository.UserRepository;
import ge.gsg.test.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;

import java.util.Optional;

import static ge.gsg.test.model.enums.GeneralErrorEnum.*;

@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private PasswordEncoder encoder;

    @PostConstruct
    public void init(){
        encoder = new BCryptPasswordEncoder();
    }

    @Override
    public void registerUser(RegisterUserRequest request) {
        try{
            log.info("Registering appUser for request: {}", request);
            Optional<AppUser> existingUser = userRepository.findByUsername(request.getUsername());
            if(existingUser.isPresent()){
                throw new GeneralException(GeneralErrorEnum.USER_ALREADY_EXIST.getCode(), GeneralErrorEnum.USER_ALREADY_EXIST.getMessage());
            }
            AppUser appUser = new AppUser();
            appUser.setJobRunInterval(request.getInterval());
            appUser.setCountry(request.getCountry());
            appUser.setUsername(request.getUsername());
            appUser.setPassword(encoder.encode(request.getPassword()));
            userRepository.save(appUser);
        }catch (GeneralException e){
            log.error("Error while registering appUser", e);
            throw e;
        }
        catch (Exception e){
            log.error("Error while registering appUser", e);
            throw new GeneralException(REGISTRATION_FAILURE.getCode(), REGISTRATION_FAILURE.getMessage());
        }
    }

    @Override
    public void updateUser(Long userId, UpdateUserRequest request) {
        log.info("Updating appUser with Id : {}", userId);
        try{
            if(request.getInterval()== null && StringUtils.isEmpty(request.getCountry())){
                throw new GeneralException(UPDATE_INVALID_PARAMS.getCode(), UPDATE_INVALID_PARAMS.getMessage());
            }
            Optional<AppUser> appUserOptional = userRepository.findById(userId);
            AppUser appUser = appUserOptional.orElseThrow(UserNotFoundException::new);
            if(!StringUtils.isEmpty(request.getCountry())){
                appUser.setCountry(request.getCountry());
            }else{
                appUser.setJobRunInterval(request.getInterval());
            }
            userRepository.save(appUser);

        }catch (UserNotFoundException e){
            log.error("user with given id not found");
            throw e;
        }catch (GeneralException e){
            log.error("Error while updating user with id : {}", userId);
            throw e;
        }
        catch (Exception e){
            log.error("Error while updating user with id : {}", userId);
            throw new GeneralException(UPDATE_FAILURE.getCode(), UPDATE_FAILURE.getMessage());
        }

    }

    @Override
    public UserResponse loginUser(LoginUserRequest request) {
        log.info("Login for user request : {}", request);
        try{
            Optional<AppUser> user = userRepository.findByUsername(request.getUsername());
            if(!user.isPresent()){
                throw new UserNotFoundException();
            }
            if(!encoder.matches(request.getPassword(), user.get().getPassword())){
                throw new InvalidUsernamePasswordException();
            }
            return user.map(UserResponse::fromAppUser).get();
        }catch (UserNotFoundException e){
            log.error("User not found for given username : {}",request.getUsername(), e);
            throw e;
        }catch (InvalidUsernamePasswordException e){
            log.error("Error while matching passwords", e);
            throw new GeneralException(GeneralErrorEnum.INVALID_PASSWORD.getCode(), GeneralErrorEnum.INVALID_PASSWORD.getMessage());
        }catch (Exception e){
            log.error("Error while logging in", e);
            throw new GeneralException(GeneralErrorEnum.LOGIN_ERROR.getCode(), GeneralErrorEnum.LOGIN_ERROR.getMessage());
        }
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}
