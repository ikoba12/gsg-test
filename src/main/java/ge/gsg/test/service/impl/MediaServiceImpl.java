package ge.gsg.test.service.impl;

import ge.gsg.test.model.entity.Media;
import ge.gsg.test.model.enums.GeneralErrorEnum;
import ge.gsg.test.model.exception.GeneralException;
import ge.gsg.test.model.request.MediaRequest;
import ge.gsg.test.model.response.MediaResponse;
import ge.gsg.test.repository.MediaRepository;
import ge.gsg.test.service.MediaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MediaServiceImpl implements MediaService {

    private MediaRepository mediaRepository;

    @Override
    public List<MediaResponse> list(MediaRequest request) {
        try{
            log.info("Getting media for request : {}", request);
            List<Media> list = mediaRepository.findAllByAppUserId(request.getUserId());
            return list.stream().map(media -> new MediaResponse(media.getMediaLink())).collect(Collectors.toList());
        }catch (Exception e){
            log.error("Error while getting media", e);
            throw new GeneralException(GeneralErrorEnum.MEDIA_GET_FAILURE.getCode(), GeneralErrorEnum.MEDIA_GET_FAILURE.getMessage());
        }
    }

    @Autowired
    public void setMediaRepository(MediaRepository mediaRepository) {
        this.mediaRepository = mediaRepository;
    }
}
