package ge.gsg.test.service;

import ge.gsg.test.model.request.MediaRequest;
import ge.gsg.test.model.response.MediaResponse;

import java.util.List;

public interface MediaService {

    List<MediaResponse> list(MediaRequest request);
}
