package ge.gsg.test.service;

import ge.gsg.test.model.entity.AppUser;

public interface MediaDownloadService {
    void downloadMediaForUser(AppUser appUser);
}
