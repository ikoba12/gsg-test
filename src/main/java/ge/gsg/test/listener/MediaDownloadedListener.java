package ge.gsg.test.listener;

import ge.gsg.test.model.entity.AppUser;
import ge.gsg.test.model.entity.Media;
import ge.gsg.test.model.events.MediaDownloadedEvent;
import ge.gsg.test.repository.MediaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class MediaDownloadedListener {

    private MediaRepository mediaRepository;

    @EventListener
    public void handleContextStart(MediaDownloadedEvent event) {
        try{
            log.info("Creating media entity for event : {}" ,event);
            Media media = new Media();
            media.setAppUser(new AppUser(event.getUserId()));
            media.setMediaLink(event.getVideoUrl());
            mediaRepository.save(media);
        }catch (Exception e){
            log.error("Error while saving media entity", e);
        }

    }

    @Autowired
    public void setMediaRepository(MediaRepository mediaRepository) {
        this.mediaRepository = mediaRepository;
    }
}
