package ge.gsg.test.controller;

import ge.gsg.test.model.request.MediaRequest;
import ge.gsg.test.model.response.MediaResponse;
import ge.gsg.test.service.MediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/media")
public class MediaController {

    private MediaService mediaService;

    @GetMapping
    public List<MediaResponse> list(@RequestParam MediaRequest request){
        return mediaService.list(request);
    }

    @Autowired
    public void setMediaService(MediaService mediaService) {
        this.mediaService = mediaService;
    }
}
