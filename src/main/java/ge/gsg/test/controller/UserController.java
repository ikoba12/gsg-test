package ge.gsg.test.controller;

import ge.gsg.test.model.exception.InvalidUsernamePasswordException;
import ge.gsg.test.model.request.LoginUserRequest;
import ge.gsg.test.model.request.RegisterUserRequest;
import ge.gsg.test.model.request.UpdateUserRequest;
import ge.gsg.test.model.response.UserResponse;
import ge.gsg.test.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {


    private UserService userService;

    @PostMapping("/login")
    private UserResponse login(@Valid @RequestBody LoginUserRequest request) throws InvalidUsernamePasswordException {
        return userService.loginUser(request);
    }

    @PostMapping("/register")
    public void registerUser(@Valid @RequestBody RegisterUserRequest request) {
        userService.registerUser(request);
    }

    @PutMapping("/{id}")
    public void updateUser(@PathVariable("id") Long userId, @Valid @RequestBody UpdateUserRequest request){
        userService.updateUser(userId, request);
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
